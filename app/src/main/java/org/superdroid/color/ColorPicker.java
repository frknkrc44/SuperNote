package org.superdroid.color;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import org.superdroid.notepad.CommonLayouts;
import org.superdroid.notepad.DrawEditor;
import org.superdroid.tab.SuperTab.BarDefaults;

public class ColorPicker {

    public static final String COLOR_SELECTED = "org.superdroid.color.clrSelected",
            COLOR_NOT_SELECTED = "org.superdroid.color.clrNotSelected";
    private static final int m = 0x44000000;
    private static final int x = 0xFFFF0000;
    private static int r, g, b;
    private final Context c;
    private final CommonLayouts cl;
    private final int min = 90;
    private Dialog d;
    private boolean ch = false;
    private View w;

    public ColorPicker(Context ctx) {
        c = ctx;
        cl = new CommonLayouts(ctx);
    }

    public static int getDefaultColor() {
        return x;
    }

    public Dialog getColorPickerDialog(int startColor) {
        return d = new AlertDialog.Builder(c).setView(createColorSelector(startColor, true, false)).create();
    }

    public int getCurrentColorValue() {
        return Color.rgb(r, g, b);
    }

    public void setColorToDefault(View colorSelectorView) {
        setColorToDefault(colorSelectorView, false);
    }

    public void setColorToDefault(View colorSelectorView, boolean dialog) {
        ((ViewGroup) colorSelectorView).getChildAt(0).setBackgroundColor(x);
        for (int i = 1; i <= 3; i++)
            ((SeekBar) ((ViewGroup) colorSelectorView).getChildAt(i)).setProgress(i == 1 ? (r = 255) : (g = b = 0));
        if (!dialog)
            c.sendBroadcast(new Intent(COLOR_SELECTED));
    }

    public View createColorSelector(int startColor) {
        return createColorSelector(startColor, false, false);
    }

    @SuppressLint("SetTextI18n")
    public View createColorSelector(int startColor, final boolean dialog, final boolean addStrokeRadius) {
        final LinearLayout ll = new LinearLayout(c);
        ll.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        ll.setOrientation(LinearLayout.VERTICAL);
        final TextView v = new TextView(c);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(-1, BarDefaults.getInfo(c, BarDefaults.HEIGHT)), lp2;
        lp.leftMargin = lp.rightMargin = (lp.height / 3);
        lp.topMargin = lp.bottomMargin = lp.leftMargin / 2;
        v.setLayoutParams(lp);
        v.setTypeface(CommonLayouts.tf());
        v.setTextSize(CommonLayouts.textSize1());
        v.setGravity(Gravity.CENTER);
        v.setBackgroundColor((startColor != -1 ? startColor : (startColor = 0xFFFF0000)) - m);
        v.setTextColor(Color.rgb(r - min, g - min, b - min));
        v.setText("#" + Integer.toHexString(startColor).toUpperCase());
        RelativeLayout rl = new RelativeLayout(c);
        rl.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        LinearLayout.LayoutParams xp;
        xp = (LinearLayout.LayoutParams) rl.getLayoutParams();
        xp.leftMargin = xp.rightMargin = lp.leftMargin;
        rl.addView(v);
        if (addStrokeRadius) {
            w = new View(c);
            w.setLayoutParams(new RelativeLayout.LayoutParams((int) (lp.height * 0.7), (int) (lp.height * 0.7)));
            w.setScaleX((DrawEditor.stroke + 1) * 0.01f);
            w.setScaleY(w.getScaleX());
            w.setBackgroundDrawable(cl.createRadiusDrawable(Color.rgb(r - min, g - min, b - min) - m, 200));
            RelativeLayout.LayoutParams rp;
            (rp = (RelativeLayout.LayoutParams) w.getLayoutParams()).addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
            rp.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
            rp.rightMargin = lp.leftMargin;
            rl.addView(w);
        }
        ll.addView(rl);
        for (int t = 0; t <= 2; t++) {
            SeekBar sb = new SeekBar(c);
            sb.setLayoutParams(new LinearLayout.LayoutParams(-1, BarDefaults.getInfo(c, BarDefaults.HEIGHT) / 2));
            lp2 = (LinearLayout.LayoutParams) sb.getLayoutParams();
            lp2.leftMargin = lp2.rightMargin = (lp.topMargin / 2);
            if (t == 2) lp2.bottomMargin = lp2.leftMargin;
            sb.setMax(255);
            sb.setProgressDrawable(cl.changeDrawableColor(sb.getProgressDrawable(), 0xFFFFFFFF));
            sb.setThumb(cl.changeDrawableColor(sb.getThumb(), 0xFFFFFFFF));
            sb.setProgress(t == 0 ? Color.red(startColor) :
                    (t == 1 ? Color.green(startColor) :
                            Color.blue(startColor)));
            sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar p1, int p2, boolean p3) {
                    if (ch) {
                        int clr;
                        v.setBackgroundColor((clr = Color.rgb(
                                r = ((SeekBar) ll.getChildAt(1)).getProgress(),
                                g = ((SeekBar) ll.getChildAt(2)).getProgress(),
                                b = ((SeekBar) ll.getChildAt(3)).getProgress()
                        )) - m);
                        v.setText("#" + Integer.toHexString(clr).toUpperCase());
                        v.setTextColor(Color.rgb(r - min, g - min, b - min) - m);
                        if (addStrokeRadius)
                            w.setBackgroundDrawable(cl.createRadiusDrawable(Color.rgb(r - min, g - min, b - min) - m, 200));
                        if (!dialog)
                            c.sendBroadcast(new Intent(COLOR_SELECTED));
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar p1) {
                    ch = true;
                }

                @Override
                public void onStopTrackingTouch(SeekBar p1) {
                    ch = false;
                }
            });
            ll.addView(sb);
        }
        if (dialog) {
            LinearLayout hl = new LinearLayout(c);
            hl.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            for (int n = 0; n < 2; n++) {
                Button bu = new Button(c);
                bu.setLayoutParams(new LinearLayout.LayoutParams(-1, -2, 1));
                bu.setTag(n);
                bu.setOnClickListener(v1 -> {
                    c.sendBroadcast(new Intent((int) v1.getTag() == 0 ? COLOR_SELECTED : COLOR_NOT_SELECTED));
                    d.cancel();
                });
                bu.setText(n == 0 ? c.getResources().getString(android.R.string.ok) :
                        c.getResources().getString(android.R.string.cancel));
                hl.addView(bu);
            }
            ll.addView(hl);
        }
        return ll;
    }

    public View getStrokeRadius() {
        return w;
    }
}
