package org.superdroid.notepad;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowInsets;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import org.superdroid.SNApplication;
import org.superdroid.color.ColorPicker;
import org.superdroid.tab.SuperTab.BarDefaults;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class DrawEditor extends Activity {

    public static int stroke = 5, color = 0xFFFF0000;
    CommonLayouts cl;
    String title;
    boolean night = false, nd = true, view = false, erase = false, pressure = false;
    long time;
    Toast t;
    ColorPicker cp;
    RelativeLayout m;
    View v, cs;
    private String[] values = null;
    private boolean bo = true;
    private float x = -1, y = -1, ox = -1, oy = -1;
    private Bitmap b = null;
    private Canvas c = null;
    private Paint p = null;
    private ImageView ll = null, xx = null;

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);
        stroke = 5;
        /*
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        */

        cl = new CommonLayouts(this);
        cp = new ColorPicker(this);
        t = cl.customToast(this, Toast.LENGTH_SHORT);
        read();
        setContentView(m = createMainView());
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(0);
            getWindow().setNavigationBarColor(0);
            getWindow().getDecorView().setFitsSystemWindows(false);
            m.setFitsSystemWindows(false);
        }

        setStatusBarTransparent();
    }

    private void setStatusBarTransparent() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

            window.setStatusBarColor(0);
        }
    }

    private RelativeLayout createMainView() {
        RelativeLayout rl = new RelativeLayout(this);
        rl.setLayoutParams(new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT));
        ll = new ImageView(this);
        ll.setLayoutParams(rl.getLayoutParams());
        ll.setScaleType(ImageView.ScaleType.FIT_CENTER);
        ll.setImageBitmap(b);
        rl.addView(ll);
        final View x = cp.createColorSelector(-1, false, true), w = cp.getStrokeRadius();
        int clr;
        rl.addView(v = cl.actionBar(title, clr = getResources().getColor(R.color.textcolor2) - 0x88FFFFFF,
                !view
                        ? cl.addMenuButton(R.drawable.color, v -> toggleColorSelector())
                        : cl.addSpace(), cl.addSpace(),
                !view
                        ? cl.addMenuButton(R.drawable.delete, v -> toggleEraseMode())
                        : cl.addSpace(), cl.addSpace(),
                (!view
                        ? cl.addMenuButton(R.drawable.save, v -> save())
                        : cl.addMenuButton(R.drawable.edit, v -> {
                    finish();
                    startActivity(new Intent(DrawEditor.this, DrawEditor.class).
                            putExtra(CommonLayouts.NOTE_ACTION, CommonLayouts.ACTION_EDIT_NOTE).
                            putExtra(CommonLayouts.NOTE_ITEMS, new String[]{values[0], values[1], values[2]}));
                })), cl.addSpace(),
                cl.addMenuButton(R.drawable.cancel, v -> finish())));
        cl.hideMenuItemBg((ViewGroup) v);
        cl.getMenuItem((ViewGroup) v, 1).setOnLongClickListener(v -> {
            clear();
            return true;
        });
        cl.getMenuItem((ViewGroup) v, 0).setOnLongClickListener(v -> {
            if (!x.isShown())
                cp.setColorToDefault(x);
            return !x.isShown();
        });
        LinearLayout xl = new LinearLayout(this);
        xl.setOrientation(LinearLayout.VERTICAL);
        xl.setVisibility(View.GONE);
        RelativeLayout.LayoutParams rp = new RelativeLayout.LayoutParams(-1, -2);
        rp.topMargin = CommonLayouts.actionBarHeight();
        xl.setLayoutParams(rp);
        xl.setBackgroundColor(((ColorDrawable) v.getBackground()).getColor());
        SeekBar sb = new SeekBar(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(((ViewGroup) x).getChildAt(1).getLayoutParams());
        lp.leftMargin = lp.rightMargin = BarDefaults.getInfo(this, BarDefaults.HEIGHT) / 6;
        sb.setLayoutParams(lp);
        sb.setProgressDrawable(cl.changeDrawableColor(sb.getProgressDrawable(), 0xFFFFFFFF));
        sb.setThumb(cl.changeDrawableColor(sb.getThumb(), 0xFFFFFFFF));
        sb.setMax(99);
        sb.setProgress(stroke);
        sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar p1, int p2, boolean p3) {
                if (w != null) {
                    stroke = p1.getProgress() + 1;
                    w.setScaleX((stroke + 1) * 0.01f);
                    w.setScaleY(w.getScaleX());
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar p1) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar p1) {
                p.setStrokeWidth(stroke = p1.getProgress() + 1);
            }
        });
        xl.addView(sb);
        xl.addView(x);
        rl.addView(xl);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(clr);
            getWindow().setNavigationBarColor(clr);
            setTaskDescription(new ActivityManager.TaskDescription(title,
                    BitmapFactory.decodeResource(getResources(),
                            R.mipmap.ic_launcher), clr + 0x88FFFFFF));
        }
        return rl;
    }

    private void toggleColorSelector() {
        (cs = m.getChildAt(2)).setVisibility(cs.isShown() ? View.GONE : View.VISIBLE);
    }

    private void toggleEraseMode() {
        if (b != null && p != null) {
            if (m.getChildAt(2).isShown()) toggleColorSelector();
            erase = !erase;
            xx = (ImageView) cl.getMenuItem((ViewGroup) v, 1);
            if (erase) {
                p.setStrokeWidth(stroke * 5);
                p.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                xx.setImageDrawable(cl.changeDrawableColor(xx.getDrawable(), 0xFF00DD00));
            } else {
                p.setStrokeWidth(stroke);
                p.setXfermode(null);
                xx.setImageDrawable(cl.changeDrawableColor(xx.getDrawable(), 0));
            }
        } else {
            assert p != null;
            p.setStrokeWidth(stroke);
            p.setXfermode(null);
            if (erase)
                xx.setImageDrawable(cl.changeDrawableColor(xx.getDrawable(), 0));
            erase = false;
        }
    }

    @SuppressLint("StringFormatMatches")
    private void read() {
        night = SNApplication.getSettingsDB().getBoolean(CommonLayouts.SETTINGS_NIGHT_MODE, false);
        pressure = SNApplication.getSettingsDB().getBoolean(CommonLayouts.SETTINGS_PRESSURE_MODE, false);
        getWindow().setBackgroundDrawable(new ColorDrawable(!night ? 0xFFFFFFFF : getResources().getColor(R.color.textcolor2)));
        Intent i;
        switch (Objects.requireNonNull((i = getIntent()).getStringExtra(CommonLayouts.NOTE_ACTION))) {
            case CommonLayouts.ACTION_VIEW_NOTE:
                view = true;
                values = i.getStringArrayExtra(CommonLayouts.NOTE_ITEMS);
                assert values != null;
                title = String.format(getString(R.string.view_note, values[0]));
                time = Long.parseLong(values[2].replace(CommonLayouts.NOTE_ITEM, ""));
                if (values[1] != null) b = BitmapFactory.decodeFile(values[1]);
                else finish();
                return;
            case CommonLayouts.ACTION_EDIT_NOTE:
                nd = false;
                values = i.getStringArrayExtra(CommonLayouts.NOTE_ITEMS);
                assert values != null;
                title = String.format(getString(R.string.edit_note, values[0]));
                time = Long.parseLong(values[2].replace(CommonLayouts.NOTE_ITEM, ""));
                if (values[1] != null) b = BitmapFactory.decodeFile(values[1]);
                else finish();
                break;
            case CommonLayouts.ACTION_NEW_NOTE:
                title = getString(R.string.new_draw);
                time = System.currentTimeMillis();
                break;
            default:
                finish();
                return;
        }
        if (!view)
            registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context p1, Intent p2) {
                    if (p != null) p.setColor(color = cp.getCurrentColorValue());
                    else set();
                }
            }, new IntentFilter(ColorPicker.COLOR_SELECTED));
        set();
    }

    private void clear() {
        ll.setImageBitmap(b = null);
        toggleEraseMode();
        nd = true;
        set();
    }

    private void set() {
        android.util.Pair<Integer, Integer> size = CommonLayouts.getDisplaySize(this);
        c = new Canvas();
        if (b == null)
            b = Bitmap.createBitmap(size.first, size.second, Bitmap.Config.ARGB_8888);
        if (!b.isMutable())
            b = b.copy(Bitmap.Config.ARGB_8888, true);
        p = new Paint();
        p.setColor(color);
        p.setStrokeWidth(stroke);
        p.setFilterBitmap(true);
        p.setAntiAlias(true);
        p.setStyle(Paint.Style.STROKE);
        p.setStrokeJoin(Paint.Join.ROUND);
        p.setStrokeCap(Paint.Cap.ROUND);
        c.setBitmap(b);
    }

    private void save() {
        if (b != null && !nd) {
            String x = getFilesDir().getAbsolutePath().replace("files", "") + "drawings/";
            File f = new File(x);
            f.mkdirs();
            f = new File(x = (x + "draw" + time));

            try {
                FileOutputStream fos = new FileOutputStream(f);
                b.compress(Bitmap.CompressFormat.PNG, 80, fos);
                fos.flush();
                fos.close();
            } catch (Exception ignored) {
            }

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
            if (SNApplication.getDrawsDB().isDBContainsKey(CommonLayouts.NOTE_ITEM + time)) {
                SNApplication.getDrawsDB().removeKeyFromDB(CommonLayouts.NOTE_ITEM + time);
                time = System.currentTimeMillis();
            }
            SNApplication.getDrawsDB().putStringArray(CommonLayouts.NOTE_ITEM + time, new String[]{
                    sdf.format(new Date(time)), x, time + ""}, true);
            cl.setCustomToastTextAndShow(t, getString(R.string.saved));
            sendBroadcast(new Intent(CommonLayouts.LOCAL_UPDATE));
            finish();
        } else {
            cl.setCustomToastTextAndShow(t, getString(R.string.mustdrawed));
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!view) {
            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_MOVE:
                case MotionEvent.ACTION_HOVER_MOVE:
                case MotionEvent.ACTION_SCROLL:
                case MotionEvent.ACTION_DOWN:
                    nd = false;
                    if (m.getChildAt(2).isShown())
                        runOnUiThread(this::toggleColorSelector);
                    if (bo) {
                        x = event.getX();
                        y = event.getY();
                        if (ox == -1) ox = x + (stroke % 25);
                        if (oy == -1) oy = y + (stroke % 25);
                        bo = false;
                    } else {
                        draw(!pressure ? -1 : event.getSize() * 100f);
                        ox = x;
                        oy = y;
                        bo = true;
                    }
                    break;
                case MotionEvent.ACTION_OUTSIDE:
                case MotionEvent.ACTION_HOVER_EXIT:
                case MotionEvent.ACTION_UP:
                    x = y = ox = oy = -1;
                    bo = true;
                    break;
            }
        }
        return super.onTouchEvent(event);
    }

    private void draw(float pre) {
        p.setStrokeWidth(pressure ? stroke + (pre / 2.5f) : stroke);
        c.drawLine(ox, oy, x, y, p);
        ll.setImageBitmap(b);
    }
}
