package org.superdroid.notepad;

import org.frknkrc44.minidb.SuperMiniDB;
import org.json.JSONArray;
import org.json.JSONException;
import org.superdroid.SNApplication;

public class SuperDBHelper {
    private SuperDBHelper() {}

    public static String[] getNotesStringArrayAlt(String name, String[] def) {
        return getStringArrayAlt(SNApplication.getNotesDB(), name, def);
    }

    public static String[] getDrawsStringArrayAlt(String name, String[] def) {
        return getStringArrayAlt(SNApplication.getDrawsDB(), name, def);
    }

    public static String[] getSettingsStringArrayAlt(String name, String[] def) {
        return getStringArrayAlt(SNApplication.getSettingsDB(), name, def);
    }

    public static String[] getStringArrayAlt(SuperMiniDB db, String name, String[] def) {
        /*
        String str = db.getString(name, "");
        if (str.isEmpty()) {
            return def;
        }

        try {
            JSONArray array = new JSONArray(str);
            String[] out = new String[array.length()];
            for (int i = 0; i < out.length; i++) {
                out[i] = array.getString(i);
            }
            return out;
        } catch(Throwable ignored) {}

        return def;

         */
        return db.getStringArray(name, def);
    }

    public static void putNotesStringArrayAlt(String name, String[] value) {
        putStringArrayAlt(SNApplication.getNotesDB(), name, value);
    }

    public static void putStringArrayAlt(SuperMiniDB db, String name, String[] value) {
        db.putStringArray(name, value);
        /*
        try {
            db.putString(name, new JSONArray(value).toString(), true);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

         */
    }
}
