package org.superdroid.notepad;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Pair;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CommonLayouts {

    public static final String ACTION_NEW_NOTE = "org.superdroid.notepad.new";
    public static final String ACTION_EDIT_NOTE = "org.superdroid.notepad.edit";
    public static final String ACTION_VIEW_NOTE = "org.superdroid.notepad.view";
    public static final String ACTION_EDIT_FILE = "org.superdroid.notepad.efile";
    public static final String NOTE_ACTION = "action";
    public static final String NOTE_ITEMS = "items";
    public static final String NOTE_ITEM = "note";
    public static final String DB_NAME = "notes";
    public static final String SET_DB_NAME = "settings";
    public static final String DRW_DB_NAME = "drawings";
    public static final String REFRESH_LIST = "refresh";
    public static final String DB_REMOVED = "dbremoved";
    public static final int DB_SEC_LEVEL = 99999;
    public static final int MAX_NOTE_LIST_SIZE = 2048;
    public static final String LOCAL_UPDATE = "superdroid.note.updateList";
    public static final String SETTINGS_NIGHT_MODE = "nightmode";

	/*public static final String ACTION_UPLOAD_TO_CLOUD = "uploadtocloud";
	public static final String ACTION_SYNC_CLOUD = "synccloud";
	public static final String ACTION_DOWNLOAD_FROM_CLOUD = "downloadfromcloud";
	public static final String ACTION_DELETE_FROM_CLOUD = "deletefromcloud";
	public static final String ACTION_CONTROL_TOKEN = "ctrltoken";
	public static final String CLOUD_ACTIONS = "cloud";
	public static final String CLOUD_ANSWER_OK = "ok";
	public static final String CLOUD_ANSWER_ERR = "error";

	//public final String CLOUD_WEBSITE = "http://haydo.esy.es/workspace/notebook/";
	public static final String CLOUD_UPLOAD_PHP = "add.php";
	public static final String CLOUD_DOWNLOAD_PHP = "get.php";
	public static final String CLOUD_DELETE_PHP = "del.php";
	public static final String CLOUD_CTRL_PHP = "ctrl.php";
	public static final String CLOUD_TOKEN = "cloudtoken";
	public static final String CLOUD_WEBSITE = "cloudaddress";
	public static final String CLOUD_TOKEN_FALSE = "Invalid token";*/
    public static final String SETTINGS_HTML_MODE = "htmlmode";
    public static final String SETTINGS_PRESSURE_MODE = "pressuremode";
    Context c;
    private Dialog d;
	
	/*public String getUserWebsite(){
		set.onlyRead();
		return set.getString(CLOUD_WEBSITE,"");
	}
	
	public String getUserToken(){
		set.onlyRead();
		return set.getString(CLOUD_TOKEN,"");
	}*/
    private Handler h = new Handler();
    private Runnable r = new Runnable() {
        public void run() {
            d.cancel();
        }
    };

    public CommonLayouts(Context ctx) {
        c = ctx;
    }

    public static int menuItemSize() {
        return (int) (actionBarHeight() * 0.75);
    }

    public static int menuItemPadding() {
        return (int) (actionBarHeight() * 0.0625);
    }

    public static int dp(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    // https://stackoverflow.com/questions/14341041/how-to-get-real-screen-height-and-width/23861333#23861333
    public static Pair<Integer, Integer> getDisplaySize(Activity context) {
        Display display = context.getWindowManager().getDefaultDisplay();
        int realWidth;
        int realHeight;

        if (Build.VERSION.SDK_INT >= 17){
            Point point = new Point();
            display.getRealSize(point);
            realWidth = point.x;
            realHeight = point.y;

            /*
            //new pleasant way to get real metrics
            DisplayMetrics realMetrics = new DisplayMetrics();
            display.getRealMetrics(realMetrics);
            realWidth = realMetrics.widthPixels;
            realHeight = realMetrics.heightPixels;

             */

        } else {
            //reflection for this weird in-between time
            try {
                Method mGetRawH = Display.class.getMethod("getRawHeight");
                Method mGetRawW = Display.class.getMethod("getRawWidth");
                realWidth = (Integer) mGetRawW.invoke(display);
                realHeight = (Integer) mGetRawH.invoke(display);
            } catch (Exception e) {
                //this may not be 100% accurate, but it's all we've got
                realWidth = display.getWidth();
                realHeight = display.getHeight();
            }

        }

        return new Pair<>(realWidth, realHeight);
    }

    public static int actionBarHeight() {
        return dp(56);//(int)(((getDeviceHeight(false) > getDeviceWidth() ? getDeviceHeight(false) : getDeviceWidth())/100)*6.5f);
    }
	
	/*public String getCloudTempFile(){
		return c.getDataDir()+"/cloud_temp";
	}*/

    public static int textSize1() {
        return (int) (actionBarHeight() * 0.15);//((actionBarHeight()*0.4)/dm.density);
    }

    public static int textSize2() {
        return (int) (textSize1() * 0.8);
    }

    public static int textSize3() {
        return (int) (textSize2() * 0.85);
    }

    public static Typeface tf() {
        return Typeface.create("sans-serif-light", Typeface.NORMAL);
    }

    public static Drawable getSelectableItemBg(Context c) {
        return csibg(c, c.getResources().getDrawable(
                c.getTheme().obtainStyledAttributes(R.style.AppTheme,
                        new int[]{android.R.attr.selectableItemBackground}
                ).getResourceId(0, 0)), false);
    }

    public static Drawable getABItemBg(Context c) {
        return csibg(c, c.getResources().getDrawable(
                c.getTheme().obtainStyledAttributes(R.style.AppTheme,
                        new int[]{android.R.attr.actionBarItemBackground}
                ).getResourceId(0, 0)), true);
    }

    private static Drawable csibg(Context c, Drawable d, boolean b) {
        int color = c.getResources().getColor(b ? R.color.textcolor2 : R.color.edittext);
        if (Build.VERSION.SDK_INT >= 21)
            ((RippleDrawable) d).setColor(new ColorStateList(new int[][]{
                    new int[]{android.R.attr.state_enabled}},
                    new int[]{color}));
        else ((StateListDrawable) d).setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        return d;
    }
	
	/*public int sp(int value){
		return (int) (value * c.getResources().getDisplayMetrics().scaledDensity);
	}*/

    public String getShareTempFile() {
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy_hh-mm-ss");
        String s = c.getExternalCacheDir().getAbsolutePath().replace("/cache", "");
        File f = new File(s);
        f.mkdirs();
        return s + "/SuperNote_" + df.format(new Date()) + ".txt";
    }

    View getMenuItem(ViewGroup act, int num) {
        return ((ViewGroup) act.getChildAt(1)).getChildAt(num * 2);
    }

    void showHideMenuItems(ViewGroup act, boolean show, int... childNumber) {
        ViewGroup vg = (ViewGroup) act.getChildAt(1);
        if (childNumber[0] != -1)
            for (int i = 0; i != childNumber.length; i++)
                vg.getChildAt(childNumber[i]).setVisibility(show ? View.VISIBLE : View.INVISIBLE);
        else vg.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
    }

    void changeActionBarColor(ViewGroup act, int color) {
        act.setBackgroundColor(color);
        act.getChildAt(1).setBackgroundColor(color);
    }

    void hideMenuItemBg(ViewGroup act) {
        act.getChildAt(1).setBackground(null);
    }

    void setActionBarTitle(ViewGroup act, String title) {
        ((TextView) act.getChildAt(0)).setText(title);
    }

    RelativeLayout actionBar(String title, int bg, View... extras) {
        int height = actionBarHeight();
        RelativeLayout rl = new RelativeLayout(c);
        rl.setLayoutParams(new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, height));
        // That's for fix a bug, DON'T DELETE IT!
        rl.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
        rl.setPadding((height / 4) + 6, 0, (height / 6) + 6, 0);
        rl.setBackgroundColor(bg);
        TextView abt = new TextView(c);
        abt.setLayoutParams(new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT));
        abt.setTypeface(Typeface.create("sans-serif-light", Typeface.NORMAL));
        abt.setTextSize(textSize1());
        abt.setTextColor(0xFFFFFFFF);
        abt.setText(title);
        abt.setSingleLine();
        abt.setGravity(Gravity.CENTER_VERTICAL);
        rl.addView(abt);
        if (extras.length > 0) {
            RelativeLayout.LayoutParams rp = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT);
            rp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
            LinearLayout ll = new LinearLayout(c);
            ll.setGravity(Gravity.CENTER_VERTICAL);
            ll.setLayoutParams(rp);
            ll.setBackgroundColor(bg);
            for (View v : extras)
                ll.addView(v);
            rl.addView(ll);
        }
        rl.setGravity(Gravity.CENTER_VERTICAL);
        return rl;
    }

    public Bitmap textAsBitmap(String text, float textSize, int textColor) {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setTextSize(textSize);
        paint.setColor(textColor);
        paint.setTextAlign(Paint.Align.LEFT);
        float baseline = -paint.ascent(); // ascent() is negative
        int width = (int) (paint.measureText(text) + 0.5f); // round
        int height = (int) (baseline + paint.descent() + 0.5f);
        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(image);
        canvas.drawText(text, 0, baseline, paint);
        return image;
    }

    Bitmap circle(Bitmap bitmap) {
        Bitmap circleBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        BitmapShader shader = new BitmapShader(bitmap, BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
        Paint paint = new Paint();
        paint.setShader(shader);
        paint.setAntiAlias(true);
        Canvas c = new Canvas(circleBitmap);
        c.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, bitmap.getWidth() / 2, paint);
        return circleBitmap;
    }

    public int getColor(int colorNum) {
        return R.color.maincolor1 + colorNum;
    }

    public Bitmap changeBitmapColor(Bitmap sourceBitmap, int color, int... wh) {
        Bitmap resultBitmap = Bitmap.createBitmap(sourceBitmap, 0, 0, wh.length < 2 ? sourceBitmap.getWidth() - 1 : wh[0], wh.length < 2 ? sourceBitmap.getHeight() - 1 : wh[1]);
        Paint p = new Paint();

        ColorFilter filter = new LightingColorFilter(c.getResources().getColor(getColor(color)), 1);
        p.setColorFilter(filter);

        Canvas canvas = new Canvas(resultBitmap);
        canvas.drawBitmap(resultBitmap, 0, 0, p);

        return resultBitmap;
    }

    public Drawable changeDrawableColor(Drawable d, int color) {
        d.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        return d;
    }

    public Dialog getCurrentDialog() {
        return d;
    }

    public void createMessageDialog(String title, String message, boolean night, View.OnClickListener... ocl) {
        LinearLayout ll = new LinearLayout(c);
        ll.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        ll.setBackground(createRadiusDrawable(night ? c.getResources().getColor(R.color.textcolor2) : c.getResources().getColor(android.R.color.white), 16));
        int p = textSize1();
        ll.setPadding(p * 2, p * 2, p, p * 2);
        ll.setOrientation(LinearLayout.VERTICAL);
        if (title != "" && title != null) {
            TextView tt = new TextView(c);
            tt.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT));
            tt.setText(title);
            tt.setSingleLine(true);
            tt.setPadding(p, 0, 0, p);
            tt.setTextSize(18);
            tt.setTextColor(night ? 0xFFFFFFFF : c.getResources().getColor(R.color.textcolor2));
            ll.addView(tt);
        }
        TextView tv = new TextView(c);
        tv.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        tv.setTypeface(Typeface.create("sans-serif-light", Typeface.NORMAL));
        tv.setText(message);
        tv.setTextSize(16);
        tv.setPadding(p * 2, p, p * 2, p);
        tv.setGravity(Gravity.CENTER);
        tv.setTextColor(night ? 0xFFFFFFFF : c.getResources().getColor(R.color.textcolor2));
        ll.addView(tv);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout ll2 = new LinearLayout(c);
        ll2.setLayoutParams(lp);
        ll2.setPadding(0, p, 0, 0);
        ll2.setGravity(Gravity.RIGHT);
        if (ocl.length == 1) {
            TextView bt = createButton(c.getResources().getString(android.R.string.cancel), night, ocl[0]);
            ll2.addView(bt);
        } else if (ocl.length == 2) {
            TextView bt2 = createButton(c.getResources().getString(android.R.string.cancel), night, ocl[1]);
            ll2.addView(bt2);
            ll2.addView(addSpace());
            TextView bt = createButton(c.getResources().getString(android.R.string.ok), night, ocl[0]);
            ll2.addView(bt);
            View v = new View(c);
            v.setLayoutParams(new LinearLayout.LayoutParams(
                    32, LinearLayout.LayoutParams.MATCH_PARENT));
            ll2.addView(v);
        }
        ll.addView(ll2);
        d = new Dialog(c);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.getWindow().setBackgroundDrawable(createRadiusDrawable(c.getResources().getColor(android.R.color.transparent), 16));
        d.setContentView(ll);
        d.show();
        if (ocl.length < 1)
            h.postDelayed(r, 5000);
    }

    public TextView createButton(String str, boolean night, View.OnClickListener ocl) {
        final TextView bt = new TextView(c);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        bt.setLayoutParams(lp);
        bt.setTextSize(18);
        bt.setTextColor(night ? 0xFFFFFFFF : c.getResources().getColor(R.color.textcolor2));
        bt.setPadding(textSize1(), textSize2(), textSize1(), textSize2());
        bt.setText(str.toUpperCase());
        bt.setGravity(Gravity.CENTER);
        bt.setOnClickListener(ocl);
        bt.setBackground(getSelectableItemBg(c));
        return bt;
    }

    public Drawable createRadiusDrawable(int color, float radius, int... stroke) {
        GradientDrawable gd = new GradientDrawable();
        gd.setColors(new int[]{color, color});
        if (stroke.length > 0 && stroke.length == 2)
            gd.setStroke(stroke[0], stroke[1]);
        gd.setCornerRadii(new float[]{radius, radius, radius, radius, radius, radius, radius, radius});
        return gd;
    }

    public ImageView addMenuButton(int resource, View.OnClickListener ocl) {
        RelativeLayout.LayoutParams rp = new RelativeLayout.LayoutParams(menuItemSize(), menuItemSize());
        final ImageView iv1 = new ImageView(c);
        iv1.setLayoutParams(rp);
        iv1.setImageResource(resource);
        iv1.setScaleType(ImageView.ScaleType.FIT_CENTER);
        int p = menuItemPadding();
        iv1.setPadding(p, p, p, p);
        iv1.setOnClickListener(ocl);
        iv1.setBackground(getABItemBg(c));
        return iv1;
    }

    public View addSpace() {
        View v1 = new View(c);
        v1.setLayoutParams(new RelativeLayout.LayoutParams(
                menuItemPadding() * 3, menuItemPadding() * 3));
        return v1;
    }

    Switch changeSwitchColor(Switch sw, int color) {
        Drawable d = sw.getThumbDrawable();
        d.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        sw.setThumbDrawable(d);
        d = sw.getTrackDrawable();
        d.setColorFilter(color - 0x44000000, PorterDuff.Mode.SRC_ATOP);
        sw.setTrackDrawable(d);
        return sw;
    }

    Switch addSwitch(String text) {
        return addSwitch(text, null, c.getResources().getColor(getColor(7)));
    }

    Switch addSwitch(String text, int color) {
        return addSwitch(text, null, color);
    }

    Switch addSwitch(String text, CompoundButton.OnCheckedChangeListener occ, int color) {
        Switch sw = new Switch(c);
        sw.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        sw.setTypeface(tf());
        sw.setTextSize(textSize2());
        sw.setText(text);
        sw.setSwitchMinWidth(textSize1() * 3);
        sw.setPadding(sw.getPaddingLeft(), sw.getPaddingTop(), sw.getPaddingRight(), sw.getPaddingBottom() + textSize1());
        sw.setTextOff(Build.VERSION.SDK_INT < 20 ? "   " : "");
        sw.setTextOn(sw.getTextOff());
        sw.setOnCheckedChangeListener(occ);
        sw.setThumbResource(R.drawable.switch_thumb);
        sw.setTrackResource(R.drawable.switch_track);
        sw = changeSwitchColor(sw, color);
        return sw;
    }

    public void createShareDialog(View[] views, boolean night, View.OnClickListener... ocl) {
        LinearLayout ll = new LinearLayout(c);
        ll.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        ll.setBackground(createRadiusDrawable(night ? c.getResources().getColor(R.color.textcolor2) : c.getResources().getColor(android.R.color.white), 16));
        int p = textSize1();
        ll.setPadding(p * 2, p * 2, p * 2, p);
        ll.setOrientation(LinearLayout.VERTICAL);

        for (View x : views)
            ll.addView(x);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout ll2 = new LinearLayout(c);
        ll2.setLayoutParams(lp);
        ll2.setPadding(p, p, p, p);
        ll2.setGravity(Gravity.RIGHT);
        if (ocl.length == 1) {
            TextView bt = createButton(c.getResources().getString(android.R.string.cancel), night, ocl[0]);
            ll2.addView(bt);
        } else if (ocl.length == 2) {
            TextView bt2 = createButton(c.getResources().getString(android.R.string.cancel), night, ocl[1]);
            ll2.addView(bt2);
            ll2.addView(addSpace());
            TextView bt = createButton(c.getResources().getString(android.R.string.ok), night, ocl[0]);
            ll2.addView(bt);
            View v = new View(c);
            v.setLayoutParams(new LinearLayout.LayoutParams(
                    32, LinearLayout.LayoutParams.MATCH_PARENT));
            ll2.addView(v);
        }
        ll.addView(ll2);
        d = new Dialog(c);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.getWindow().setBackgroundDrawable(createRadiusDrawable(c.getResources().getColor(android.R.color.transparent), 16));
        d.setContentView(ll);
        d.show();
        if (ocl.length < 1)
            h.postDelayed(r, 5000);
    }

    RelativeLayout shareItem(final String title, final boolean night, int color, View.OnClickListener ocl) {
        final RelativeLayout rl = new RelativeLayout(c);
        rl.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                (int) (menuItemSize() * 1.75)));
        rl.setGravity(Gravity.CENTER_VERTICAL);
        rl.setPadding(menuItemPadding() * 3, menuItemPadding() * 3, menuItemPadding() * 3, menuItemPadding() * 3);
        ImageView iv = new ImageView(c);
        iv.setLayoutParams(new RelativeLayout.LayoutParams(menuItemSize(), RelativeLayout.LayoutParams.MATCH_PARENT));
        iv.setScaleType(ImageView.ScaleType.FIT_CENTER);
        iv.setPadding(menuItemPadding(), menuItemPadding(), menuItemPadding(), menuItemPadding());
        Bitmap b = BitmapFactory.decodeResource(c.getResources(), R.drawable.share);
        iv.setImageBitmap(changeBitmapColor(b, color));
        rl.addView(iv);
        LinearLayout ll = new LinearLayout(c);
        ll.setLayoutParams(new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT));
        ll.setPadding(menuItemSize() + (menuItemPadding() * 2), 0, menuItemSize() + (menuItemPadding() * 2), 0);
        ll.setOrientation(LinearLayout.VERTICAL);
        ll.setGravity(Gravity.CENTER_VERTICAL);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        TextView t1 = new TextView(c);
        t1.setLayoutParams(lp);
        t1.setGravity(Gravity.CENTER_VERTICAL);
        t1.setTypeface(tf());
        t1.setTextSize(textSize2());
        t1.setText(title.length() > 0 ? title : c.getResources().getString(R.string.notext));
        t1.setSingleLine();
        t1.setTextColor(night ? c.getResources().getColor(R.color.textcolor1) : c.getResources().getColor(R.color.textcolor2));
        ll.addView(t1);
        rl.addView(ll);
        rl.setOnClickListener(ocl);
        rl.setBackground(getSelectableItemBg(c));
        return rl;
    }

    public LinearLayout toastCustomView(String text) {
        LinearLayout ll = new LinearLayout(c);
        ll.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        ll.setBackground(createRadiusDrawable(R.color.edittext, 8));
        TextView tv = new TextView(c);
        tv.setLayoutParams(ll.getLayoutParams());
        tv.setTextSize(textSize2());
        tv.setText(text);
        tv.setTypeface(tf());
        ll.addView(tv);
        return ll;
    }

    public Toast customToast(Context c, int length) {
        Toast t = Toast.makeText(c, "", length);
        t.setView(toastView(c));
        t.setGravity(Gravity.CENTER, 0, 0);
        return t;
    }

    public void setCustomToastText(Toast t, String text) {
        LinearLayout ll = (LinearLayout) t.getView();
        TextView tv = (TextView) ll.getChildAt(0);
        tv.setText(text);
    }

    public void setCustomToastTextAndShow(Toast t, String text) {
        setCustomToastText(t, text);
        t.show();
    }

    private LinearLayout toastView(Context c) {
        LinearLayout ll = new LinearLayout(c);
        ll.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        ll.setOrientation(LinearLayout.VERTICAL);
        ll.setBackground(createRadiusDrawable(c.getResources().getColor(R.color.edittext), 16f));
        TextView tv = new TextView(c);
        tv.setLayoutParams(ll.getLayoutParams());
        tv.setTypeface(tf());
        tv.setTextSize(textSize1());
        tv.setGravity(Gravity.CENTER);
        tv.setTextColor(0xFFFFFFFF);
        int p = textSize1();
        tv.setPadding(p, p, p, p);
        ll.addView(tv);
        return ll;
    }

}
