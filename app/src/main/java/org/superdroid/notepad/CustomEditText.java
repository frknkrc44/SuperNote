package org.superdroid.notepad;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.EditText;
import android.widget.TextView;

import java.lang.reflect.Field;

public class CustomEditText extends EditText {
    public CustomEditText(Context context) {
        super(context);
        setBg();
        setCursor();
        setLightFonts(true);
        setTextSize(CommonLayouts.textSize2());
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setBg();
        setCursor();
        setLightFonts(true);
        setTextSize(CommonLayouts.textSize2());
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setBg();
        setCursor();
        setLightFonts(true);
        setTextSize(CommonLayouts.textSize2());
    }

    public void setTextToTop(boolean value) {
        setGravity(value ? Gravity.TOP : Gravity.CENTER_VERTICAL);
    }

    public void setLightFonts(boolean value) {
        setTypeface(Typeface.create(value ? "sans-serif-light" : "sans-serif", Typeface.NORMAL));
    }

    public boolean usingLightFonts() {
        return getTypeface() == Typeface.create("sans-serif-light", Typeface.NORMAL);
    }

    public void setBg() {
        setBackgroundResource(R.drawable.etboxbg);
    }

	@SuppressLint("SoonBlockedPrivateApi")
	public void setCursor() {
        try {
            Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
            f.setAccessible(true);
            f.set(this, R.drawable.etcursor);
        } catch (Exception ignored) {}
    }
}
