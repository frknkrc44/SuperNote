package org.superdroid.notepad;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.superdroid.SNApplication;
import org.superdroid.tab.SuperTab;

import java.io.File;
import java.util.Objects;

public class NotesList extends Activity {

    CommonLayouts cl;
    RelativeLayout main, ml;
    Toast t;
    SuperTab st;
    Switch sw, pm;
    View ab;
    float x;
    Handler h = new Handler();
    BroadcastReceiver br = new BroadcastReceiver() {
        @Override
        public void onReceive(Context p1, Intent p2) {
            final Intent i = p2;
            h.post(() -> {
                switch (Objects.requireNonNull(i.getAction())) {
                    case CommonLayouts.LOCAL_UPDATE:
                        apply();
                        break;
                    case SuperTab.SELITEMCHANGED:
                        String tit;
                        cl.setActionBarTitle((ViewGroup) ab, tit = getString(st.getSelected() == 0 ? R.string.noteslist : (st.getSelected() == 1 ? R.string.drawings : R.string.settings)));
                        cl.showHideMenuItems((ViewGroup) ab, st.getSelected() != 2, -1);
                        int clr;
                        cl.changeActionBarColor((ViewGroup) ab, clr = getResources().getColor(cl.getColor(st.getSelected() == 0 ? 7 : (st.getSelected() == 1 ? 6 : 8))));
                        /*sw = cl.changeSwitchColor(sw,clr);
                        pm = cl.changeSwitchColor(pm,clr);*/
                        st.setBackgroundColor(clr);
                        if (Build.VERSION.SDK_INT >= 21) {
                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                            getWindow().setStatusBarColor(clr);
                            getWindow().setNavigationBarColor(clr);
                            NotesList.this.setTaskDescription(new ActivityManager.TaskDescription(tit,
                                    BitmapFactory.decodeResource(getResources(),
                                            R.mipmap.ic_launcher), clr));
                        }
                        break;
                }

            });
        }
    };
    boolean exit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cl = new CommonLayouts(this);
        t = cl.customToast(this, Toast.LENGTH_LONG);
        setContentView(mainLayout());
        st = new SuperTab(this, ml, SuperTab.DEFVALUE,
                (int) (CommonLayouts.actionBarHeight() * 1.25),
                getResources().getColor(cl.getColor(7)));
        st.addButton(R.drawable.save);
        st.addButton(R.drawable.color);
        st.addButton(R.drawable.settings);
        View y = st.getBar();
        RelativeLayout.LayoutParams z = new RelativeLayout.LayoutParams(y.getLayoutParams());
        z.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        y.setLayoutParams(z);
        main.addView(y);
        IntentFilter i = new IntentFilter(CommonLayouts.LOCAL_UPDATE);
        i.addAction(SuperTab.SELITEMCHANGED);
        registerReceiver(br, i);
    }

    RelativeLayout mainLayout() {
        main = new RelativeLayout(this);
        main.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));

        ImageView iv1 = cl.addMenuButton(R.drawable.addnote,
                v -> startActivity(
                        new Intent(NotesList.this, st.getSelected() == 0
                                ? NoteEditor.class
                                : DrawEditor.class)
                                .putExtra(CommonLayouts.NOTE_ACTION, CommonLayouts.ACTION_NEW_NOTE)));

        main.addView(ab = cl.actionBar(getResources().getString(R.string.noteslist),
                getResources().getColor(R.color.maincolor8), iv1));
        main.addView(fillNotesList(getNightMode()));

        if (Build.VERSION.SDK_INT >= 21)
            NotesList.this.setTaskDescription(new ActivityManager.TaskDescription(
                    getResources().getString(R.string.noteslist),
                    BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher),
                    getResources().getColor(R.color.maincolor8)));
        return main;
    }

    View fillNotesList(boolean night) {
        getWindow().setBackgroundDrawable(new ColorDrawable(night ? getResources().getColor(R.color.textcolor2) : Color.WHITE));
        ml = new RelativeLayout(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        lp.bottomMargin = SuperTab.BarDefaults.getInfo(this, SuperTab.BarDefaults.HEIGHT);
        ml.setLayoutParams(lp);
        ml.setPadding(0, CommonLayouts.actionBarHeight(), 0, 0);
        LinearLayout xl = new LinearLayout(this);
        xl.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        ScrollView sv = new ScrollView(this);
        sv.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        LinearLayout ll = new LinearLayout(this);
        ll.setLayoutParams(new ScrollView.LayoutParams(
                ScrollView.LayoutParams.MATCH_PARENT,
                ScrollView.LayoutParams.MATCH_PARENT));
        ll.setOrientation(LinearLayout.VERTICAL);
        for (String key : SNApplication.getNotesDB().getKeys(true)) {
            String[] n = SuperDBHelper.getNotesStringArrayAlt(key, new String[]{});
            if (n.length != 0)
                ll.addView(noteListItem(n[0], n[1], Integer.parseInt(n[2]),
                        Long.parseLong(key.replace(CommonLayouts.NOTE_ITEM, "")), night));
        }
        if (ll.getChildCount() == 0) {
            TextView tv = new TextView(this);
            tv.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT));
            tv.setGravity(Gravity.CENTER);
            tv.setPadding(0, CommonLayouts.actionBarHeight() / 2, 0, 0);
            tv.setText(getResources().getString(R.string.listempty));
            tv.setTypeface(CommonLayouts.tf());
            tv.setTextSize(CommonLayouts.textSize2());
            tv.setTextColor(night ? getResources().getColor(R.color.textcolor1) : getResources().getColor(R.color.textcolor2));
            ll.addView(tv);
        }
        sv.addView(ll);
        xl.addView(sv);
        ml.addView(xl);
        ml.addView(fillDrawList(night));
        ml.addView(getSettings());
        return ml;
    }

    View fillDrawList(boolean night) {
        LinearLayout ll = new LinearLayout(this);
        ll.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        ll.setOrientation(LinearLayout.VERTICAL);
        for (String key : SNApplication.getDrawsDB().getKeys(true)) {
            String[] n = SNApplication.getDrawsDB().getStringArray(key, new String[]{});
            if (n.length != 0)
                ll.addView(drawListItem(n[0], n[1], Long.parseLong(key.replace(CommonLayouts.NOTE_ITEM, "")), night));
        }
        if (ll.getChildCount() == 0) {
            TextView tv = new TextView(this);
            tv.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT));
            tv.setGravity(Gravity.CENTER);
            tv.setPadding(0, CommonLayouts.actionBarHeight() / 2, 0, 0);
            tv.setText(getResources().getString(R.string.dlistempty));
            tv.setTypeface(CommonLayouts.tf());
            tv.setTextSize(CommonLayouts.textSize2());
            tv.setTextColor(night ? getResources().getColor(R.color.textcolor1) : getResources().getColor(R.color.textcolor2));
            ll.addView(tv);
        }
        ScrollView sv = new ScrollView(this);
        sv.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        sv.addView(ll);
        return sv;
    }

    LinearLayout getSettings() {
        LinearLayout ll = new LinearLayout(this);
        ll.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        ll.setOrientation(LinearLayout.VERTICAL);
        int pad = CommonLayouts.textSize1() * 2;
        ll.setPadding(pad, pad, pad, pad);
        sw = cl.addSwitch(getResources().getString(R.string.nightmode), getResources().getColor(cl.getColor(8)));
        sw.setChecked(getNightMode());
        sw.setOnCheckedChangeListener((p1, p2) -> {
            SNApplication.getSettingsDB().putBoolean(CommonLayouts.SETTINGS_NIGHT_MODE, p2, true);
            apply();
        });
        sw.setTextColor(sw.isChecked() ? Color.WHITE : getResources().getColor(R.color.textcolor2));
        pm = cl.addSwitch(getResources().getString(R.string.pressmode), getResources().getColor(cl.getColor(8)));
        pm.setChecked(getPressureMode());
		/*pm.setOnTouchListener(new View.OnTouchListener(){
			@Override
			public boolean onTouch(View v, MotionEvent e){
				if(ort(e.getPressure()) >= 0.6){
					cl.setCustomToastTextAndShow(t,"Your screen don't support this feature"+ort(x));
					v.setTag(false);
				} else v.setTag(true);
				return false;
			}
		});*/
        pm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton p1, boolean p2) {
                //if((boolean)p1.getTag()){
                SNApplication.getSettingsDB().putBoolean(CommonLayouts.SETTINGS_PRESSURE_MODE, p2, true);
                apply();
                //} else p1.setChecked(false);
            }
        });
        pm.setTextColor(sw.isChecked() ? Color.WHITE : getResources().getColor(R.color.textcolor2));
        ll.addView(sw);
        ll.addView(pm);
        return ll;
    }

    public float ort(float f) {
        return x = (f + x) / 2;
    }

    View drawListItem(final String title, final String file, final long noteNum, final boolean night) {
        final RelativeLayout rl = new RelativeLayout(this);
        rl.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                (int) (CommonLayouts.menuItemSize() * 1.75)));
        rl.setGravity(Gravity.CENTER);
        int pxd = CommonLayouts.menuItemPadding();
        int pad = pxd * 3;
        rl.setPadding(pad, pad, pad, pad);
        ImageView iv = new ImageView(this);
        iv.setLayoutParams(new RelativeLayout.LayoutParams(CommonLayouts.menuItemSize(), RelativeLayout.LayoutParams.MATCH_PARENT));
        iv.setScaleType(ImageView.ScaleType.FIT_CENTER);
        iv.setPadding(pxd, pxd, pxd, pxd);
        Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.color);
        iv.setImageBitmap(cl.changeBitmapColor(b, 6));
        rl.addView(iv);
        RelativeLayout.LayoutParams rp2 = new RelativeLayout.LayoutParams(CommonLayouts.menuItemSize(), RelativeLayout.LayoutParams.MATCH_PARENT);
        rp2.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        ImageView iv2 = new ImageView(this);
        iv2.setLayoutParams(rp2);
        Bitmap bi = BitmapFactory.decodeResource(getResources(), R.drawable.delete);
        iv2.setImageBitmap(cl.changeBitmapColor(bi, night ? 9 : 10));
        iv2.setScaleType(ImageView.ScaleType.FIT_CENTER);
        iv2.setPadding(pxd, pxd, pxd, pxd);
        iv2.setOnClickListener(v -> cl.createMessageDialog(
                getResources().getString(R.string.warning),
                getResources().getString(R.string.delete),
                getNightMode(), v1 -> {
                    if (SNApplication.getDrawsDB().getLength() == 1) {
                        String x = file.substring(0, file.lastIndexOf("/"));
                        File f = new File(x);
                        for (String s : f.list())
                            new File(f, s).delete();
                        f.delete();
                        SNApplication.getDrawsDB().removeDB();
                    } else {
                        SNApplication.getDrawsDB().removeKeyFromDB(CommonLayouts.NOTE_ITEM + noteNum);
                        new File(file).delete();
                    }
                    SNApplication.getDrawsDB().refresh();
                    sendBroadcast(new Intent(CommonLayouts.LOCAL_UPDATE));
                    t.setDuration(Toast.LENGTH_SHORT);
                    cl.setCustomToastTextAndShow(t, String.format(getResources().getString(R.string.ddeleted), "1"));
                    t.setDuration(Toast.LENGTH_LONG);
                    cl.getCurrentDialog().cancel();
                },
                v12 -> cl.getCurrentDialog().cancel()));
        TextView t1 = new TextView(this);
        t1.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        t1.setPadding(CommonLayouts.menuItemSize() + (pxd * 2), 0, CommonLayouts.menuItemSize() + (pxd * 2), 0);
        t1.setText(!title.isEmpty() ? title : getResources().getString(R.string.notext));
        t1.setTextColor(night ? getResources().getColor(R.color.textcolor1) : getResources().getColor(R.color.textcolor2));
        t1.setTypeface(CommonLayouts.tf());
        t1.setGravity(Gravity.CENTER_VERTICAL);
        rl.addView(t1);
        rl.setOnClickListener(v -> startActivity(new Intent(NotesList.this, DrawEditor.class).
                putExtra(CommonLayouts.NOTE_ACTION, CommonLayouts.ACTION_VIEW_NOTE).
                putExtra(CommonLayouts.NOTE_ITEMS, new String[]{title, file, "" + noteNum})));
        rl.setBackground(CommonLayouts.getSelectableItemBg(NotesList.this));
        rl.addView(iv2);
        return rl;
    }

    RelativeLayout noteListItem(final String title, final String note, final int colorNum, final long noteNum, final boolean night) {
        final RelativeLayout rl = new RelativeLayout(this);
        rl.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                (int) (CommonLayouts.menuItemSize() * 1.75)));
        rl.setGravity(Gravity.CENTER);
        int pxd = CommonLayouts.menuItemPadding();
        int pad = pxd * 3;
        rl.setPadding(pad, pad, pad, pad);
        ImageView iv = new ImageView(this);
        iv.setLayoutParams(new LinearLayout.LayoutParams(CommonLayouts.menuItemSize(), RelativeLayout.LayoutParams.MATCH_PARENT));
        iv.setScaleType(ImageView.ScaleType.FIT_CENTER);
        iv.setPadding(pxd, pxd, pxd, pxd);
        Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.edit);
        iv.setImageBitmap(cl.changeBitmapColor(b, colorNum));
        rl.addView(iv);
        RelativeLayout.LayoutParams rp2 = new RelativeLayout.LayoutParams(CommonLayouts.menuItemSize(), RelativeLayout.LayoutParams.MATCH_PARENT);
        rp2.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        ImageView iv2 = new ImageView(this);
        iv2.setLayoutParams(rp2);
        Bitmap bi = BitmapFactory.decodeResource(getResources(), R.drawable.delete);
        iv2.setImageBitmap(cl.changeBitmapColor(bi, night ? 9 : 10));
        iv2.setScaleType(ImageView.ScaleType.FIT_CENTER);
        iv2.setPadding(pxd, pxd, pxd, pxd);
        iv2.setOnClickListener(v -> cl.createMessageDialog(
                getResources().getString(R.string.warning),
                getResources().getString(R.string.delete),
                getNightMode(), v1 -> {
                    if (SNApplication.getNotesDB().getLength() == 1)
                        SNApplication.getNotesDB().removeDB();
                    else
                        SNApplication.getNotesDB().removeKeyFromDB(CommonLayouts.NOTE_ITEM + noteNum);
                    SNApplication.getNotesDB().refresh();
                    sendBroadcast(new Intent(CommonLayouts.LOCAL_UPDATE));
                    t.setDuration(Toast.LENGTH_SHORT);
                    cl.setCustomToastTextAndShow(t, String.format(getResources().getString(R.string.deleted), "1"));
                    t.setDuration(Toast.LENGTH_LONG);
                    cl.getCurrentDialog().cancel();
                },
                v12 -> cl.getCurrentDialog().cancel()));
        LinearLayout ll = new LinearLayout(this);
        ll.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        ll.setPadding(CommonLayouts.menuItemSize() + (CommonLayouts.menuItemPadding() * 2), 0, CommonLayouts.menuItemSize() + (CommonLayouts.menuItemPadding() * 2), 0);
        ll.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, 1);
        TextView t1 = new TextView(this);
        t1.setLayoutParams(lp);
        TextView t2 = new TextView(this);
        t2.setLayoutParams(lp);
        t1.setTypeface(CommonLayouts.tf());
        t1.setGravity(Gravity.CENTER_VERTICAL);
        t2.setGravity(Gravity.TOP);
        t2.setTypeface(CommonLayouts.tf());
        t1.setTextSize(CommonLayouts.textSize2());
        t2.setTextSize(CommonLayouts.textSize3());
        t1.setText(!title.isEmpty() ? title : getResources().getString(R.string.notext));
        t2.setText(!note.isEmpty() ? note : getResources().getString(R.string.notext));
        t1.setSingleLine();
        t2.setSingleLine();
        t1.setTextColor(night ? getResources().getColor(R.color.textcolor1) : getResources().getColor(R.color.textcolor2));
        t2.setTextColor(night ? getResources().getColor(R.color.textcolor1) : getResources().getColor(R.color.textcolor2));
        ll.addView(t1);
        ll.addView(t2);
        rl.addView(ll);
        rl.setOnClickListener(v -> startActivity(new Intent(NotesList.this, NoteEditor.class).
                putExtra(CommonLayouts.NOTE_ACTION, CommonLayouts.ACTION_VIEW_NOTE).
                putExtra(CommonLayouts.NOTE_ITEMS, new String[]{title, note, "" + colorNum, "" + noteNum})));
        rl.setBackground(CommonLayouts.getSelectableItemBg(NotesList.this));
        rl.addView(iv2);
        return rl;
    }

    void apply() {
        main.removeView(ml);
        main.addView(fillNotesList(getNightMode()), 1);
        st.setRootView(ml);
    }

    boolean getNightMode() {
        return SNApplication.getSettingsDB().getBoolean(CommonLayouts.SETTINGS_NIGHT_MODE, false);
    }

    boolean getPressureMode() {
        return SNApplication.getSettingsDB().getBoolean(CommonLayouts.SETTINGS_PRESSURE_MODE, false);
    }

    @Override
    public void onBackPressed() {
        if (exit) {
            t.cancel();
            super.onBackPressed();
        } else {
            exit = true;
            cl.setCustomToastTextAndShow(t, getResources().getString(R.string.exit));
            h.postDelayed(() -> exit = false, 3000);
        }
    }
}
