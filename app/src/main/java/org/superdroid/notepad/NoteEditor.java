package org.superdroid.notepad;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.superdroid.SNApplication;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

public class NoteEditor extends Activity {

    private CommonLayouts cl;
    private String ntitle, nnote, abTitle;
    private int abColor;
    private long noteNumber;
    private CustomEditText et1, et2;
    private Toast t;
    private boolean view, exit;
    private Random r = new Random();

    @Override
    public void onCreate(Bundle b) {
        super.onCreate(b);
        cl = new CommonLayouts(this);
        t = cl.customToast(this, Toast.LENGTH_SHORT);
        getAction();
        applyColorToBars();
        setContentView(mainLayout());
    }

    private boolean getNM() {
        return SNApplication.getSettingsDB().getBoolean(CommonLayouts.SETTINGS_NIGHT_MODE, false);
    }

    private void shareAsText() {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, String.format(getResources().getString(R.string.sharedetail), ntitle, nnote));
        startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.sharemenu)));
    }

    private void shareAsFile() throws IOException {
        String note = String.format(getResources().getString(R.string.sharedetail), ntitle, nnote);
        String tempFile = cl.getShareTempFile();
        File f = new File(tempFile);
        FileOutputStream fos = new FileOutputStream(f);
        f.createNewFile();
        fos.write(note.getBytes());
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_STREAM, Uri.fromFile(new File(tempFile)));
        startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.sharemenu)));
    }

    private View[] shareItem() {
        return new View[]{
                cl.shareItem(getResources().getString(R.string.textformat), getNM(),
                        abColor, v -> {
                            shareAsText();
                            cl.getCurrentDialog().cancel();
                        }),
                cl.shareItem(getResources().getString(R.string.fileformat), getNM(),
                        abColor, v -> {
                            try {
                                shareAsFile();
                            } catch (Exception e) {
                                Toast.makeText(getBaseContext(), e.toString(), Toast.LENGTH_LONG).show();
                            }
                            cl.getCurrentDialog().cancel();
                        }),
        };
    }

    private LinearLayout mainLayout() {
        getWindow().setBackgroundDrawable(new ColorDrawable(getNM() ? getResources().getColor(R.color.textcolor2) : Color.WHITE));
        final LinearLayout ll = new LinearLayout(this);
        ll.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        ll.setOrientation(LinearLayout.VERTICAL);
        ImageView iv1 = cl.addMenuButton(R.drawable.color, v -> {
            abColor = randomClr();
            ll.getChildAt(0).setBackgroundColor(
                    getResources().getColor(cl.getColor(abColor)));
            ((RelativeLayout) ll.getChildAt(0)).getChildAt(1).
                    setBackgroundColor(getResources().getColor(cl.getColor(abColor)));
            applyColorToBars();
        });

        ImageView iv2 = cl.addMenuButton(R.drawable.save,
                v -> save(et1.getText().toString(), et2.getText().toString(), false));

        ImageView iv3 = cl.addMenuButton(R.drawable.cancel, v -> {
            exit = true;
            finish();
        });

        ImageView iv4 = cl.addMenuButton(R.drawable.edit, v -> {
            finish();
            startActivity(new Intent(NoteEditor.this, NoteEditor.class).
                    putExtra(CommonLayouts.NOTE_ACTION, CommonLayouts.ACTION_EDIT_NOTE).
                    putExtra(CommonLayouts.NOTE_ITEMS, new String[]{ntitle, nnote, "" + abColor, "" + noteNumber}));
        });

        ImageView iv5 = cl.addMenuButton(R.drawable.share,
                v -> cl.createShareDialog(shareItem(), getNM(),
                        v1 -> cl.getCurrentDialog().cancel()));

        View[] x = new View[]{cl.addSpace(), view ? iv5 : iv1, cl.addSpace(), view ? iv4 : iv2, cl.addSpace(), iv3};

        ll.addView(cl.actionBar(abTitle,
                getResources().getColor(cl.getColor(abColor)), x));
        ll.addView(view ? textViewer() : textEditor());
        return ll;
    }

    private View textViewer() {
        ScrollView sv = new ScrollView(this);
        sv.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        LinearLayout ll = new LinearLayout(this);
        ll.setLayoutParams(new ScrollView.LayoutParams(
                ScrollView.LayoutParams.MATCH_PARENT,
                ScrollView.LayoutParams.MATCH_PARENT));
        int p = cl.textSize1() * 2;
        ll.setPadding(p, p, p, p);
        ll.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        TextView t2 = new TextView(this);
        t2.setLayoutParams(lp);
        t2.setTypeface(cl.tf());
        t2.setTextIsSelectable(true);
        t2.setText(nnote.length() > 0 ? nnote : getResources().getString(R.string.notext));
        t2.setTextColor(getNM() ? Color.WHITE : getResources().getColor(R.color.textcolor2));
        t2.setTextSize(cl.textSize2());
        ll.addView(t2);
        sv.addView(ll);
        return sv;
    }

    private View textEditor() {
        LinearLayout ll = new LinearLayout(this);
        ll.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        int p = cl.textSize1() * 2;
        ll.setPadding(p, p, p, p);
        ll.setOrientation(LinearLayout.VERTICAL);
        et1 = new CustomEditText(this);
        et1.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        et1.setHint(getResources().getString(R.string.notehint1));
        et1.setText(ntitle);
        et1.setTextColor(getNM() ? Color.WHITE : getResources().getColor(R.color.textcolor2));
        et1.setHintTextColor(getNM() ? Color.WHITE - 0x88000000 : getResources().getColor(R.color.textcolor2) - 0x88000000);
        et1.setSingleLine();
        View v1 = new View(this);
        v1.setLayoutParams(new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, cl.menuItemSize() / 5));
        et2 = new CustomEditText(this);
        et2.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        et2.setHint(getResources().getString(R.string.notehint2));
        et2.setText(nnote);
        et2.setTextToTop(true);
        et2.setTextColor(getNM() ? Color.WHITE : getResources().getColor(R.color.textcolor2));
        et2.setHintTextColor(getNM() ? Color.WHITE - 0x88000000 : getResources().getColor(R.color.textcolor2) - 0x88000000);
        ll.addView(et1);
        ll.addView(v1);
        ll.addView(et2);
        return ll;
    }

    private void save(String title, String note, boolean pause) {
        if (!exit) {
            if (!title.isEmpty() || !note.isEmpty()) {
                String noteName = CommonLayouts.NOTE_ITEM + noteNumber;
                if (SNApplication.getNotesDB().isDBContainsKey(noteName)) {
                    SNApplication.getNotesDB().removeKeyFromDB(noteName);
                    noteNumber = System.currentTimeMillis();
                    noteName = CommonLayouts.NOTE_ITEM + noteNumber;
                }
                SuperDBHelper.putNotesStringArrayAlt(noteName,
                        new String[]{title, note, "" + abColor, "" + noteNumber});
                cl.setCustomToastTextAndShow(t, getResources().getString(R.string.saved));
                if (!pause)
                    finish();
                sendBroadcast(new Intent(CommonLayouts.LOCAL_UPDATE));
            } else if (!pause)
                cl.setCustomToastTextAndShow(t, getResources().getString(R.string.mustfilled));
        }
    }

    public void applyColorToBars() {
        try {
            if (Build.VERSION.SDK_INT >= 21) {
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                getWindow().setStatusBarColor(getResources().getColor(cl.getColor(abColor)));
                getWindow().setNavigationBarColor(getResources().getColor(cl.getColor(abColor)));
                ActivityManager.TaskDescription taskDescription = new ActivityManager.TaskDescription(abTitle,
                        BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher),
                        getResources().getColor(cl.getColor(abColor)));
                NoteEditor.this.setTaskDescription(taskDescription);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int randomClr() {
        int t = r.nextInt(9);
        return t != abColor ? t : randomClr();
    }

    private void getAction() {
        switch (getIntent().getStringExtra(CommonLayouts.NOTE_ACTION)) {
            case CommonLayouts.ACTION_NEW_NOTE:
                view = false;
                ntitle = nnote = "";
                abColor = randomClr();
                abTitle = getResources().getString(R.string.new_note);
                noteNumber = System.currentTimeMillis();
                break;
            case CommonLayouts.ACTION_EDIT_NOTE:
                view = false;
                String[] noteItems = getIntent().getStringArrayExtra(CommonLayouts.NOTE_ITEMS);
                assert noteItems != null;
                ntitle = noteItems[0];
                nnote = noteItems[1];
                abColor = Integer.parseInt(noteItems[2]);
                noteNumber = Long.parseLong(noteItems[3]);
                abTitle = String.format(getResources().getString(R.string.edit_note), ntitle);
                break;
            case CommonLayouts.ACTION_VIEW_NOTE:
                view = true;
                String[] noteItem = getIntent().getStringArrayExtra(CommonLayouts.NOTE_ITEMS);
                assert noteItem != null;
                ntitle = noteItem[0];
                nnote = noteItem[1];
                abColor = Integer.parseInt(noteItem[2]);
                noteNumber = Long.parseLong(noteItem[3]);
                abTitle = String.format(getResources().getString(R.string.view_note), ntitle);
                break;
            default:
                view = false;
                Toast.makeText(getBaseContext(), getResources().getString(R.string.invalidop), Toast.LENGTH_SHORT).show();
                finish();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        exit = true;
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        try {
            save(et1.getText().toString(), et2.getText().toString(), true);
        } catch (Exception | Error ignored) {}
        super.onPause();
    }
	
	/*WebView createWebViewer(){
		
	}*/

}
