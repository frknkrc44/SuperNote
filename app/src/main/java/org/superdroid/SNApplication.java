package org.superdroid;

import android.app.Application;

import org.frknkrc44.minidb.SuperMiniDB;
import org.superdroid.notepad.CommonLayouts;

public class SNApplication extends Application {
    private static SuperMiniDB settingsDB, notesDB, drawsDB;

    public static SuperMiniDB getSettingsDB() {
        return settingsDB;
    }

    public static SuperMiniDB getNotesDB() {
        return notesDB;
    }

    public static SuperMiniDB getDrawsDB() {
        return drawsDB;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        settingsDB = new SuperMiniDB(CommonLayouts.SET_DB_NAME, getFilesDir());
        notesDB = new SuperMiniDB(CommonLayouts.DB_NAME, getFilesDir());
        drawsDB = new SuperMiniDB(CommonLayouts.DRW_DB_NAME, getFilesDir());
    }
}
