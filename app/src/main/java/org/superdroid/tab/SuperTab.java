package org.superdroid.tab;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class SuperTab {
    public static final int DEFVALUE = 0x98, DEFCOLOR = 0xFF444444;
    public static final String SELITEMCHANGED = "selchanged";
    private Context c;
    private LinearLayout mainLayout;
    private int numLast = 0, oldSelected = 0;
    private float disabled = 0.6f;
    private ViewGroup root;

    public SuperTab(Context ctx) {
        createNewBar(ctx, null, BarDefaults.getInfo(ctx, BarDefaults.WIDTH), BarDefaults.getInfo(ctx, BarDefaults.HEIGHT), BarDefaults.getInfo(ctx, BarDefaults.BGCOLOR), BarDefaults.getInfo(ctx, BarDefaults.SELECTION));
    }

    public SuperTab(Context ctx, ViewGroup rootView) {
        createNewBar(ctx, rootView, BarDefaults.getInfo(ctx, BarDefaults.WIDTH), BarDefaults.getInfo(ctx, BarDefaults.HEIGHT), BarDefaults.getInfo(ctx, BarDefaults.BGCOLOR), BarDefaults.getInfo(ctx, BarDefaults.SELECTION));
    }

    public SuperTab(Context ctx, ViewGroup rootView, int height) {
        if (height == DEFVALUE) height = BarDefaults.getInfo(ctx, BarDefaults.HEIGHT);
        createNewBar(ctx, rootView, BarDefaults.getInfo(ctx, BarDefaults.WIDTH), height, BarDefaults.getInfo(ctx, BarDefaults.BGCOLOR), BarDefaults.getInfo(ctx, BarDefaults.SELECTION));
    }

    public SuperTab(Context ctx, ViewGroup rootView, int width, int height) {
        if (width == DEFVALUE) width = BarDefaults.getInfo(ctx, BarDefaults.WIDTH);
        if (height == DEFVALUE) height = BarDefaults.getInfo(ctx, BarDefaults.HEIGHT);
        createNewBar(ctx, rootView, width, height, BarDefaults.getInfo(ctx, BarDefaults.BGCOLOR), BarDefaults.getInfo(ctx, BarDefaults.SELECTION));
    }

    public SuperTab(Context ctx, ViewGroup rootView, int width, int height, int bgColor) {
        if (width == DEFVALUE) width = BarDefaults.getInfo(ctx, BarDefaults.WIDTH);
        if (height == DEFVALUE) height = BarDefaults.getInfo(ctx, BarDefaults.HEIGHT);
        if (bgColor == DEFVALUE) bgColor = BarDefaults.getInfo(ctx, BarDefaults.BGCOLOR);
        createNewBar(ctx, rootView, width, height, bgColor, BarDefaults.getInfo(ctx, BarDefaults.SELECTION));
    }

    public SuperTab(Context ctx, ViewGroup rootView, int width, int height, int bgColor, int selection) {
        if (width == DEFVALUE) width = BarDefaults.getInfo(ctx, BarDefaults.WIDTH);
        if (height == DEFVALUE) height = BarDefaults.getInfo(ctx, BarDefaults.HEIGHT);
        if (bgColor == DEFVALUE) bgColor = BarDefaults.getInfo(ctx, BarDefaults.BGCOLOR);
        if (selection == DEFVALUE) selection = BarDefaults.getInfo(ctx, BarDefaults.SELECTION);
        createNewBar(ctx, rootView, width, height, bgColor, selection);
    }

    private void createNewBar(Context ctx, ViewGroup rootView, int width, int height, int bgColor, int selection) {
        c = ctx;
        mainLayout = new LinearLayout(ctx);
        mainLayout.setLayoutParams(new ViewGroup.LayoutParams(width, height));
        mainLayout.setBackgroundColor(bgColor);
        root = rootView;
        oldSelected = selection;
    }

    public View getBar() {
        if (mainLayout.getChildCount() > 0 &&
                mainLayout.getChildCount() - 1 < oldSelected)
            setSelected(mainLayout.getChildCount() - 1);
        return mainLayout;
    }

    public int getSelected() {
        return oldSelected;
    }

    public void setSelected(int selection) {
        if (root != null) {
            if (mainLayout.getChildCount() - 1 < oldSelected)
                oldSelected = selection = mainLayout.getChildCount() - 1;
            if (oldSelected != selection)
                mainLayout.getChildAt(oldSelected).animate().scaleX(disabled).scaleY(disabled).setInterpolator(new OvershootInterpolator());
            mainLayout.getChildAt(selection).animate().scaleX(1).scaleY(1).setInterpolator(new OvershootInterpolator());
            for (int i = 0; i != root.getChildCount(); i++)
                root.getChildAt(i).setVisibility(i == selection ? View.VISIBLE : View.GONE);
        }
        c.sendBroadcast(new Intent(SELITEMCHANGED));
        oldSelected = selection;
    }

    public void setSelected(boolean next) {
        if (next) {
            if (getSelected() == (mainLayout.getChildCount() - 1))
                return;
            setSelected(getSelected() + 1);
        } else {
            if (getSelected() == 0)
                return;
            setSelected(getSelected() - 1);
        }
    }

    public void setRootView(ViewGroup v) {
        root = v;
        setSelected(oldSelected);
    }

    public int getBackgroundColor() {
        return ((ColorDrawable) mainLayout.getBackground()).getColor();
    }

    public void setBackgroundColor(int bgColor) {
        mainLayout.setBackgroundColor(bgColor);
    }

    private ImageView getiv() {
        return (ImageView) mainLayout.getChildAt(mainLayout.getChildCount() - 1);
    }

    public void addButton(Drawable img) {
        buttonView();
        ImageView v;
        (v = getiv()).setImageDrawable(img);
        if (v.getTag().equals(oldSelected)) setSelected(oldSelected);
        numLast++;
    }

    public void addButton(Bitmap img) {
        buttonView();
        ImageView v;
        (v = getiv()).setImageBitmap(img);
        if (v.getTag().equals(oldSelected)) setSelected(oldSelected);
        numLast++;
    }

    public void addButton(int resource) {
        buttonView();
        ImageView v;
        (v = getiv()).setImageResource(resource);
        if (v.getTag().equals(oldSelected)) setSelected(oldSelected);
        numLast++;
    }

    private void buttonView() {
        ImageView iv = new ImageView(c);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        lp.weight = 1;
        iv.setLayoutParams(lp);
        iv.setTag(numLast);
        iv.setScaleX(disabled);
        iv.setScaleY(iv.getScaleX());
        int p = DisplayInfo.getInfo(c, DisplayInfo.MIN) / 30;
        iv.setPadding(p, p, p, p);
        iv.setScaleType(ImageView.ScaleType.FIT_CENTER);
        iv.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                setSelected((int) v.getTag());
            }
        });
        mainLayout.addView(iv);
    }

    public static class BarDefaults {
        public static final int WIDTH = 0, HEIGHT = 1, SELECTION = 2, BGCOLOR = 3;

        public static int getInfo(Context ctx, int req) {
            switch (req) {
                case WIDTH:
                    return ViewGroup.LayoutParams.MATCH_PARENT;
                case HEIGHT:
                    return (int) (DisplayInfo.getInfo(ctx, req) * 0.09);
                case SELECTION:
                    return 0;
                case BGCOLOR:
                    return DEFCOLOR;
                default:
                    return -1;
            }
        }
    }

    public static class DisplayInfo {
        public static final int WIDTH = 0, HEIGHT = 1, DENSITY = 2, MIN = 3;

        public static int getInfo(Context ctx, int req) {
            DisplayMetrics dm = new DisplayMetrics();
            ((Activity) ctx).getWindowManager().getDefaultDisplay().getMetrics(dm);
            switch (req) {
                case WIDTH:
                    return dm.widthPixels;
                case HEIGHT:
                    return dm.heightPixels;
                case DENSITY:
                    return dm.densityDpi;
                case MIN:
                    return dm.widthPixels > dm.heightPixels ? dm.heightPixels : dm.widthPixels;
                default:
                    return BarDefaults.getInfo(ctx, BarDefaults.WIDTH);
            }
        }
    }
}
